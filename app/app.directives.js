app.directive('ngNav', function() {

	return {
		restrict: 'E',
		templateUrl: 'app/shared/nav/_nav.html'
	}

});