app.controller('BookController', ['$scope', '$http', function($scope, $http) {
	$http.get('books.json').then(function(response) {
    	$scope.books = response.data;
    });

}]);