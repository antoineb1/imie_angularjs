app.controller('GetBookController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

	var id = $routeParams.index;

	$http.get('books.json').then(function(response) {
    	var datas = response.data;

    	for (var i = 0; i < datas.length; i++) {
    		if(datas[i].index == id) {
    			$scope.book = (datas[i]);
    		}
    	}
    });
}]);