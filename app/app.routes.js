app.config(function($routeProvider, $locationProvider) {
	$routeProvider
		.when('/', {templateUrl: 'app/components/book/bookView.html', controller: 'BookController'})
		.when('/getBook/:index', {templateUrl: 'app/components/getBook/getBookView.html', controller: 'GetBookController'})
		.otherwise({redirectTo : '/'})
});